<?php
/**
 * Created by PhpStorm.
 * User: vinhnguyenh1
 * Date: 7/16/2019
 * Time: 9:44 AM
 */

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;

class Author
{
    /**
     * @Assert\NotBlank
     * @Assert\Length(
     *     min = 5,
     *     max = 10,
     *     minMessage = "Your first name must be at least {{ limit }} characters long",
     *     maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     */
    public $firstName;

    /**
     * @Assert\NotBlank
     * @Assert\Length(
     *     min = 5,
     *     max = 10,
     *     minMessage = "Your last name must be at least {{ limit }} characters long",
     *     maxMessage = "Your last name cannot be longer than {{ limit }} characters"
     * )
     */
    public $lastName;

    /**
     * @Assert\NotBlank
     * @Assert\Email
     * @Assert\Length(
     *     min = 10,
     *     max = 30,
     *     minMessage = "Your email must be at least {{ limit }} characters long",
     *     maxMessage = "Your email cannot be longer than {{ limit }} characters"
     * )
     */
    public $email;

    /**
     * @Assert\NotBlank
     * @Assert\Length(
     *     min = 4,
     *     max = 8,
     *     minMessage = "Your pass code must be at least {{ limit }} characters long",
     *     maxMessage = "Your pass code cannot be longer than {{ limit }} characters"
     * )
     */
    public $code;

}