<?php
/**
 * Created by PhpStorm.
 * User: vinhnguyenh1
 * Date: 7/15/2019
 * Time: 10:56 AM
 */

namespace AppBundle\Controller;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class UserController extends Controller
{

    public function __construct()
    {

    }

    /**
     * @Route("/register", name="register")
     */
    public function registerAction(Request $request, SessionInterface $session)
    {
        $form = $this->createFormBuilder()
            ->add('name', TextType::class)
            ->add('email', EmailType::class)
            ->add('pwd', PasswordType::class)
            ->add('repwd', PasswordType::class)
            ->add('Register', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            if ($formData['pwd'] === $formData['repwd']) {
                $session->set('name', $formData['name']);
                $session->set('email', $formData['email']);
                $session->set('pwd', $formData['pwd']);
                $this->addFlash(
                    'notice',
                    'You have successfully registered'
                );
                return $this->redirectToRoute('homepage');
            } else {
                $this->addFlash(
                    'notice',
                    'Your confirm password not match'
                );
            }

        }
        return $this->render('user/register.html.twig', [
            'register_form' => $form->createView()
        ]);

    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(SessionInterface $session)
    {
        $user = [];
        $user['name'] = $session->get('name');
        $user['email'] = $session->get('email');
        $user['pwd'] = $session->get('pwd');
        return $this->render('home/home.html.twig', [
            'user' => $user,
        ]);
    }
}