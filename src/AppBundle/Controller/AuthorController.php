<?php
/**
 * Created by PhpStorm.
 * User: vinhnguyenh1
 * Date: 7/16/2019
 * Time: 9:50 AM
 */
namespace AppBundle\Controller;

use AppBundle\Entity\Author;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AuthorController extends Controller
{
    /**
     * @Route("/author", name="author")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('author/author.html.twig');
    }

    /**
     * @Route("/author/validate", name="validate-author")
     */
    public function validateAction(Request $request)
    {
        $author = new Author();
        $author->firstName = $request->request->get('firstName');
        $author->lastName = $request->request->get('lastName');
        $author->email = $request->request->get('email');
        $author->code = $request->request->get('code');

        $validator = $this->get('validator');
        $errors = $validator->validate($author);
        //var_dump($errors);
        if (count($errors) > 0) {
            return $this->render('author/author.html.twig', [
                'errors' => $errors,
                'author' => $author
            ]);
        }
        return new Response("aaa");
    }
}