<?php
/**
 * Created by PhpStorm.
 * User: hungnguyenv4
 * Date: 7/2/2019
 * Time: 2:44 PM
 */

namespace AppBundle;


class Test
{
    private $name;
    public function __construct($name)
    {
        $this->name=$name;
    }
    public function doSomething()
    {
        return "doSomething {$this->name}";
    }
}